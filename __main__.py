import menyou as m

def hello_world():
    name = input('Please enter your name: ')
    print(f'Hello world, {name}')

sub_menu = m.Menyou(
    'My Great App',
    'Sub Menu',
    [m.Opshin(name='Hello World', payload=hello_world),
     m.Opshin(name='Five times x', payload=lambda x: x*5, disabled=True)],
    'Pick a function to execute: '
)

main_menu = m.Menyou(
    'My Great App',
    'Main Menu',
    [m.Opshin(name='Sub Menu', payload=sub_menu),
    m.Opshin(name='Exit', payload=m.Menyou.exit)],
    'Pick a submenu to navigate to: '
)

main_menu.display_menu()